<?php
/*
    Plugin Name: Intelpad Common
    Description: Simple hacks to every wordpress installation
    Author: Apostolos Papastolopoulos
    Version: 0.2
    Author URI: http://intelpad.eu/
    */

  // =========================================================================
    // REMOVE JUNK FROM HEAD
    // =========================================================================

    function removeHeadLinks()
    {
      remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
      remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
      remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
      remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
      remove_action('wp_head', 'index_rel_link'); // index link
      remove_action('wp_head', 'parent_post_rel_link', 10, 0); // prev link
      remove_action('wp_head', 'start_post_rel_link', 10, 0); // start link
      remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
      remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
      remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
      remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
      remove_action('wp_head', 'print_emoji_detection_script', 7);
      remove_action('wp_print_styles', 'print_emoji_styles');
      remove_action('wp_head', 'rel_canonical', 10, 0);
      remove_action('wp_head', 'rest_output_link_wp_head', 10); // remove the REST API link
      remove_action('wp_head', 'wp_oembed_add_discovery_links'); // remove oEmbed discovery links
      remove_action('template_redirect', 'rest_output_link_header', 11, 0); // remove the REST API link from HTTP Headers
      remove_action('wp_head', 'wp_oembed_add_host_js'); // remove oEmbed-specific javascript from front-end / back-end
      remove_action('rest_api_init', 'wp_oembed_register_route'); // remove the oEmbed REST API route
      remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10); // don't filter oEmbed results
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    require_once dirname(__FILE__) . '/admin-common.php';
	require_once dirname(__FILE__) . '/common-functions.php';
    require_once dirname(__FILE__) . '/content-types.php';
    require_once dirname(__FILE__) . '/tgm-plugin-activation/init.php';
    //require_once dirname(__FILE__) . '/admin/admin-init.php';

    add_filter('jpeg_quality', create_function('', 'return 75;'));

    //paste to config.php
    //define('WP_POST_REVISIONS', 0);

