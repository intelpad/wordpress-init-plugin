<?php


add_action('wp_head', 'mbe_wp_head');
function mbe_wp_head(){
	echo '<style>'
		.PHP_EOL
		.'body.body-logged-in .navbar-fixed-top{ top: 46px !important; }'
		.PHP_EOL
		.'body.logged-in .navbar-fixed-top{ top: 46px !important; }'
		.PHP_EOL
		.'@media only screen and (min-width: 783px) {'
		.PHP_EOL
		.'body.body-logged-in .navbar-fixed-top{ top: 28px !important; }'
		.PHP_EOL
		.'body.logged-in .navbar-fixed-top{ top: 28px !important; }'
		.PHP_EOL
		.'}</style>'
		.PHP_EOL;
}


function is_staff() {

    $roles = array('administrator', 'editor');
    $id = get_current_user_id();
    $user = get_userdata($id);
    $is = 0;
    foreach ($roles as $role) {
        if (in_array($role, $user->roles))
            $is += 1;
    }
    if ($is > 0)
        return true;
    else
        return false;
}


//add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!is_staff() && !is_admin()) {
        show_admin_bar(false);
    }
}

//add_action('init', 'blockusers_init');

function blockusers_init() {
    if (is_admin() && !is_staff() &&
            !( defined('DOING_AJAX') && DOING_AJAX )) {
        wp_redirect(home_url());
        exit;
    }
}


/*
 *
 * CUSTOMISE LOGIN SCREEN
 * 
 */

function my_login_logo() {
    $upload_dir = wp_upload_dir();
    $location = $upload_dir['baseurl'];
    ?>
    <style type="text/css">
        #login{padding:5% 0 0}
        .login div#login h1 a {
            box-shadow:0px 1px 3px rgba(0, 0, 0, 0.2);
            background-color: #fff !important;
            background-image: url("<?php echo $location . '/2013/03/logo-Belgicannats.jpg'; ?>");
            width:auto;
            height:300px;
            background-size: auto;
            background-position: 30px 30px;
        }
    </style>
    <?php
}

//add_action('login_head', 'my_login_logo');


//add_action('parse_query', 'changept');

function changept() {
    if (is_category() && !is_admin())
        set_query_var('post_type', array('aga', 'post', 'efc_job', 'news', 'defacto', 'spotlight', 'tribe_events', 'effect', 'publication', 'organisation', 'thematic_network', 'session'));
    return;
}


// ADD CUSTOM QUERY VARIABLES
function add_query_vars_filter($vars) {

  $vars[] = "gallery";
  //  $vars[] = "code";
      return $vars;
}

add_filter('query_vars', 'add_query_vars_filter');


// posts per page based on CPT
function iti_custom_posts_per_page($query) {
    switch ($query->query_vars['post_type']) {
        case 'news':  // Post Type named 'iti_cpt_1'
            $query->query_vars['posts_per_page'] = 10;
            break;
        case 'thematic_network':  // Post Type named 'iti_cpt_1'
            $query->query_vars['posts_per_page'] = 20;
            break;
        case 'publication':  // Post Type named 'iti_cpt_1'
            $query->query_vars['posts_per_page'] = 24;
            break;
        case 'session':  // Post Type named 'iti_cpt_1'
            $query->query_vars['posts_per_page'] = 40;
            break;
    }
    return $query;
}

if (!is_admin()) {
    //add_filter('pre_get_posts', 'iti_custom_posts_per_page');
}



// EXAMPLE FOR AJAX FUNCTIONS
function get_acontent() {
    $id = $_POST['pid'];

    $post = get_post($id);
    $link = $post->guid;
    $text = explode('.', $post->post_content);
    $content = $text[0] . '<br/>' . $link;
    echo $content;
//var_dump($post);

    die();
}

//add_action('wp_ajax_get_acontent', 'get_acontent');
//add_action('wp_ajax_nopriv_get_acontent', 'get_acontent');



/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function update_data($post_id, $post, $update) {
    global $redux_demo;
    $efc_admins = $redux_demo['opt-admins'];
    $dafne_admins = $redux_demo['opt-dafne-admins'];
    /*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $slug = 'country_profile';

    // If this isn't a 'country_profile' post, don't update it.
    if ($slug != $post->post_type) {
        return;
    }

    $profile = get_post($post_id);
    $title = $profile->post_title;
    $date = $profile->post_date;
    $status = $profile->post_status;
    $headers = array('From: EFC website administrator <admin@efc.be>;Content-Type: text/html; charset=UTF-8');
    $body = 'Hello \r\n someone has updated the country profile of ' . $title . '. on ' . $date . ' and the profile status is set to ' . $status . '. Please go to the administrator area and syncronise the profiles http://dafne-online.eu/wp-admin/admin.php?page=dafne-syncronisation-plugin%2Fdafne_sync.php.\r\n Thank you.';
    $subject = 'A country profile has been updated in the EFC website';
    wp_mail($dafne_admins, $subject, $body, $headers);
}

//add_action('save_post', 'update_data', 10, 3);



function check_user_role($xroles, $user_id = NULL) {
    // Get user by ID, else get current user
	// User not in roles
	return FALSE;
	
    if ($user_id)
        $user = get_userdata($user_id);
    else
        $user = wp_get_current_user();

    // No user found, return
    if (empty($user))
        return FALSE;


    // Loop through user roles
    foreach ($user->roles as $role) {
        // Does user have role
        if (in_array($role, $xroles)) {
            return TRUE;
        }
    }

    
}

//exclude categories from search
function SearchFilter($query) {
  if ( $query->is_search && ! is_admin() ) {
    $query->set('cat','8,15');
  }
  return $query;
}
//add_filter('pre_get_posts','SearchFilter');


//exclude pages from search
function modify_search_filter($query) {
  if ($query->is_search) {
    $query->set('post_type', 'post');
  }
  return $query;
}

//add_filter('pre_get_posts','modify_search_filter');
